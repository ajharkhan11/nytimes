//
//  NYTimesTests.swift
//  NYTimesTests
//
//  Created by Ajharudeen Khan on 14/01/24.
//

import XCTest


class ArticleViewModelTests: XCTestCase {
    class MockArticleDelegate: ArticleDelegate {
        
        var successCalled = false
        var failureCalled = false
        var receivedError: APIError?
        
        func success() {
            successCalled = true
        }
        
        func failure(error: APIError) {
            failureCalled = true
            receivedError = error
        }
    }
    
    func testCallFuncToGetArticleDataSuccess(){
        // Arrange
        let mockWebService = MockWebService()
        let viewModel = ArticleViewModel(networkService: mockWebService)
        let mockDelegate = MockArticleDelegate()
        viewModel.delegate = mockDelegate
        
        // Set up mock data
        let expectedResponse = MockResponse()
        let mockResponseData = try? JSONEncoder().encode(expectedResponse)
        // Provide mock response data
        mockWebService.responseData = mockResponseData
        
        viewModel.callFuncToGetArticleData()
        // Simulate waiting for the asynchronous task to complete (e.g., using XCTest expectations)
        let expectation = expectation(description: "Successful call")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                expectation.fulfill()
            }
        waitForExpectations(timeout: 2)
        XCTAssertTrue(mockDelegate.successCalled)
        XCTAssertFalse(mockDelegate.failureCalled)
        
    }
    
    func testCallFuncToGetArticleDataFailure() {
        // Arrange
        let mockWebService = MockWebService()
        let viewModel = ArticleViewModel(networkService: mockWebService)
        let mockDelegate = MockArticleDelegate()
        viewModel.delegate = mockDelegate
        
        // Set up mock error
        let mockError = APIError.invalidResponse
        mockWebService.responseError = mockError
        
        viewModel.callFuncToGetArticleData()
        
        // Simulate waiting for the asynchronous task to complete (e.g., using XCTest expectations)
        let expectation = expectation(description: "Failure call")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                expectation.fulfill()
            }
        waitForExpectations(timeout: 2)
        
        XCTAssertTrue(mockDelegate.failureCalled)
        XCTAssertFalse(mockDelegate.successCalled)
    }
}
