//
//  APIServiceTests.swift
//  NYTimesTests
//
//  Created by Ajharudeen Khan on 14/01/24.
//

import XCTest
import UIKit

struct MockResponse: Codable, Equatable {
    // Define the structure of your mock response
}


final class MockWebService: WebService {
    var capturedURL: URL?
    var capturedHeaders: [String: String]?
    var responseData: Data?
    var responseError: APIError?

    func getData<T: Decodable>(url: URL?, headers: [String: String]?) async throws -> T {
        capturedURL = url
        capturedHeaders = headers

        if let responseError = responseError {
            throw responseError
        }

        guard let responseData = responseData else {
            throw APIError.invalidResponse
        }

        return try JSONDecoder().decode(T.self, from: responseData)
    }
}


class APIWebServiceTests: XCTestCase {
    
    func testGetDataSuccess() {
        // Arrange
        let mockWebService = MockWebService()
        let url = URL(string: "https://example.com")!
        let headers = ["Authorization": "Bearer Token"]
        let expectedResponse = MockResponse() // Provide mock response data
        mockWebService.responseData = try? JSONEncoder().encode(expectedResponse)

        // Act
        Task {
            do {
                let result: MockResponse = try await mockWebService.getData(url: url, headers: headers)

                // Assert
                XCTAssertEqual(result, expectedResponse)
                XCTAssertEqual(mockWebService.capturedURL, url)
                XCTAssertEqual(mockWebService.capturedHeaders, headers)
            } catch {
                XCTFail("Unexpected error: \(error)")
            }
        }
    }

    func testGetDataFailure() {
        // Arrange
        let mockWebService = MockWebService()
        let url = URL(string: "https://example.com")!
        let headers = ["Authorization": "Bearer Token"]
        let expectedError = APIError.invalidURL
        mockWebService.responseError = expectedError

        // Act
        Task {
            do {
                let result: MockResponse = try await mockWebService.getData(url: url, headers: headers)

                // Assert
                XCTFail("Unexpected Reponse Instead of Error: \(result)")
            } catch {
                XCTAssertEqual(error as? APIError, expectedError)
                XCTAssertEqual(mockWebService.capturedURL, url)
                XCTAssertEqual(mockWebService.capturedHeaders, headers)
            }
        }
    }
}
