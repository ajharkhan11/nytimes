# NYTimes
NTimes Most Popular Articles API and show a list of articles,
that shows details when items on the list are tapped (a typical master/detail app). 


## Getting started
Just clone the repo and open NYTimes.xcodeproj Project in XCode.
There are no third party dependancy So ,no need to do anything.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git clone https://gitlab.com/ajharkhan11/nytimes.git
git branch -M main
```

