//
//  ArticleDetailViewModel.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 30/01/24.
//

import Foundation

final class ArticleDetailViewModel {
    var article: Results
    
    init(article: Results) {
        self.article = article
    }
}
