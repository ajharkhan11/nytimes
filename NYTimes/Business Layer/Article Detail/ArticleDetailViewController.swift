//
//  ArticleDetailViewController.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 14/01/24.
//

import UIKit

class ArticleDetailViewController: UIViewController {

    @IBOutlet private weak var mediaView: UIImageView!
    
    @IBOutlet private weak var mediaCaption: UILabel!
    
    @IBOutlet private weak var source: UILabel!
    
    @IBOutlet private weak var publishedDate: UILabel!
    
    @IBOutlet private weak var articleTitle: UILabel!
    
    @IBOutlet private weak var articleDesc: UILabel!
    
    @IBOutlet private weak var byline: UILabel!
    
    @IBOutlet private weak var imageHeight: NSLayoutConstraint!
    
    var viewModel: ArticleDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpUI()
    }
    
    func setUpUI() {
        guard let article = viewModel?.article else {return}
        
        //setup article detail
        self.source.text = article.source
        self.articleTitle.text = article.title
        self.articleDesc.text = article.abstract
        self.byline.text = article.byline
        self.publishedDate.text = article.publishedDate
        //setup media
        
        guard let media = article.media?.first else {return}
        if let mediaUrl = media.metadata?.last?.url {
            self.mediaView.loadImageUsingCache(withUrl: mediaUrl)
        }
        
        self.mediaCaption.text = media.caption
        
    }
    
    @IBAction func browserAction(_ sender: UIBarButtonItem) {
        guard let urlString = viewModel?.article.url else {return}
        
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
