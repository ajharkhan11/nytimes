//
//  ArticleTableViewCell.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 11/01/24.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet private weak var articleTitle: UILabel!
    
    @IBOutlet private weak var articleDescription: UILabel!
    
    @IBOutlet private weak var articleImage: UIImageView!
    
    @IBOutlet private weak var articlePublished: UILabel!
    
    func configure(viewModel: ArticleTableViewCellViewModel?) {
        guard let viewModel = viewModel else {
            return
        }
        
        self.articleTitle.text = viewModel.article.title
        self.articleDescription.text = viewModel.article.byline
        self.articlePublished.text = viewModel.article.publishedDate
        if let url = viewModel.article.media?.first?.metadata?.first?.url {
            self.articleImage.loadImageUsingCache(withUrl:url)
        }
    }
    
    static func nib() -> UINib{
        UINib(nibName: "ArticleTableViewCell", bundle: nil)
    }
}
