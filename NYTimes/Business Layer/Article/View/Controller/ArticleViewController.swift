//
//  ArticleViewController.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 11/01/24.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var articleTableView: UITableView!
    
    private var viewModel: ArticleViewModel!
    
    private var dataSource : TableViewDataSourceViewModel<ArticleTableViewCell , Results>!
    
    private var cellIdenfier = "article_cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.articleTableView.register(ArticleTableViewCell.nib(), forCellReuseIdentifier: cellIdenfier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.callToViewModelForUIUpdate()
    }
    
    private func callToViewModelForUIUpdate() {
        self.viewModel = ArticleViewModel(networkService: APIWebService())
        self.viewModel.delegate = self
        self.getArticles()
    }
    
    private func getArticles() {
        self.startActivityIndicator()
        self.viewModel.callFuncToGetArticleData()
    }
    
    private func updateDataSource() {
        
        guard let articles = viewModel.articles else{
            return
        }
        
        self.dataSource = TableViewDataSourceViewModel(cellIdentifier: cellIdenfier, items: articles, configureCell: { cell, article in
            let viewModel = ArticleTableViewCellViewModel(article: article)
            cell.configure(viewModel: viewModel)
        })
        
        self.dataSource.handleCick = { item in
            self.performSegue(withIdentifier: "detail", sender: item)
        }
        
        
        self.articleTableView.dataSource = self.dataSource
        self.articleTableView.delegate = self.dataSource
        
        DispatchQueue.main.async {
            self.articleTableView.reloadData()
        }
    }
}

//MARK: API Handle

extension ArticleViewController: ArticleDelegate {
    func success() {
        self.updateDataSource()
        
        self.stopActivityIndicator()
    }
    
    func failure(error: APIError) {
        self.presentAlert(withTitle: "Error!!!", message: error.localizedDescription)
    }
}

//MARK: Navigation
extension ArticleViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let article = sender as? Results {
            let detailController = segue.destination as? ArticleDetailViewController
            let viewModel = ArticleDetailViewModel(article: article)
            detailController?.viewModel = viewModel
        }
    }
}
