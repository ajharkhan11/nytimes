//
//  Results.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.

import Foundation

struct Results : Codable, Equatable {
	let uri : String?
	let url : String?
	let id : Int?
	let asset_id : Int?
	let source : String?
	let publishedDate : String?
	let updated : String?
	let section : String?
	let subsection : String?
	let nytdsection : String?
	let adxAdxKeywordswords : String?
	let column : String?
	let byline : String?
	let type : String?
	let title : String?
	let abstract : String?
	let desFacet : [String]?
	let orgFacet : [String]?
	let perFacet : [String]?
	let geoFacet : [String]?
	let media : [Media]?
	let etaId : Int?
    
    enum CodingKeys: String, CodingKey {
        case uri
        case url
        case id
        case asset_id
        case source
        case publishedDate = "published_date"
        case updated
        case section
        case subsection
        case nytdsection
        case adxAdxKeywordswords = "adx_adx_keywordswords"
        case column
        case byline
        case type
        case title
        case abstract
        case desFacet = "des_facet"
        case orgFacet = "org_facet"
        case perFacet = "per_facet"
        case geoFacet = "geo_facet"
        case media
        case etaId = "etaId"
    }
}
