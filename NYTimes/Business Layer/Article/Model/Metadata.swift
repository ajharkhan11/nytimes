//
//  Metadata.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.

import Foundation

struct Metadata : Codable, Equatable {
	let url : String?
	let format : String?
	let height : Int?
	let width : Int?
}
