//
//  Base.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.

import Foundation

struct Base : Codable, Equatable {
	let status : String?
	let copyright : String?
	let numResults : Int?
	let results : [Results]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case copyright
        case numResults = "num_results"
        case results
    }
}
