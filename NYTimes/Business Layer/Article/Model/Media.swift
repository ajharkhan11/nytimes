//
//  Media.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.

import Foundation

struct Media : Codable, Equatable {
	let type : String?
	let subtype : String?
	let caption : String?
	let copyright : String?
	let approvedForSyndication : Int?
	let metadata : [Metadata]?

	enum CodingKeys: String, CodingKey {
		case type
		case subtype
		case caption
		case copyright
		case approvedForSyndication = "approved_for_syndication"
		case metadata = "media-metadata"
	}
}
