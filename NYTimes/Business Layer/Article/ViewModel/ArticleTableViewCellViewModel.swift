//
//  ArticleTableViewCellViewModel.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 01/02/24.
//

import Foundation

final class ArticleTableViewCellViewModel {
    var article: Results
    
    init(article: Results) {
        self.article = article
    }
}
