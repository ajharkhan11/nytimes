//
//  ArticleViewModel.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.
//

import Foundation

protocol ArticleDelegate: AnyObject {
    func success()
    func failure(error: APIError)
}


final class ArticleViewModel {
    private var networkService : WebService
    
    weak var delegate: ArticleDelegate?
    
    private(set) var articles : [Results]?
    
    init(networkService: WebService) {
        self.networkService = networkService
    }
    
    func callFuncToGetArticleData() {
        let mostViewApiEndPoint = MostViewedEndpoint(section: "all-sections", period: "7")
        Task{
            do {
                let base: Base = try await self.networkService.getData(url: mostViewApiEndPoint.makeURL(), headers: nil)
                DispatchQueue.main.async {
                    self.articles = base.results
                    self.delegate?.success()
                }
            } catch {
                DispatchQueue.main.async {
                    self.delegate?.failure(error: APIError.decodingError(error.localizedDescription))
                }
            }
        }
    }
}
