//
//  APIWebService.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.
//

import Foundation

protocol WebService {
    func getData<T: Decodable>(url: URL?, headers: [String: String]?) async throws -> T
}

struct APIWebService: WebService {
    func getData<T: Decodable>(url: URL?, headers: [String: String]? = nil) async throws -> T {
        guard let url = url else {
            throw APIError.invalidURL
        }
        
        var request = URLRequest(url: url)

        // Add headers if any
        headers?.forEach { key, value in
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        let (responseData, _) = try await URLSession.shared.data(for: request)
        
        let result = try JSONDecoder().decode(T.self, from: responseData)
        return result
    }
}
