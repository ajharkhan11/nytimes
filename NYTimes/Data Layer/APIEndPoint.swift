//
//  APIEndPoint.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 30/01/24.
//

import Foundation

protocol APIEndpoint {

    var base: String { get }
    var apiKey: String { get }
    var path: String { get }
    var parameters: [String: String]? { get }

    func makeURL() -> URL?
}

struct MostViewedEndpoint: APIEndpoint {
    
    var section: String
    
    var period: String
    
    init(section: String, period: String) {
        self.section = section
        self.period = period
    }
    var base: String {
        return "https://api.nytimes.com/svc/mostpopular/v2"
    }

    var apiKey: String {
        return "1nAc8Yp2cWwZTCRW1gXCvjsADkkBqCHi"
    }

    var path: String {
        return "/mostviewed"
    }
    
    var parameters: [String: String]? {
        return ["api-key": apiKey]
    }
    
    var format: String {
        return ".json"
    }

    func makeURL() -> URL? {
        var components = URLComponents(string: base + path + "/" + section + "/" + period + format)
        components?.queryItems = parameters?.map { URLQueryItem(name: $0, value: $1) }
        return components?.url
    }
}
