//
//  Errors.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.
//

import Foundation

struct Constant {
    static let NetworkError = "Something went wrong. Try again."
    static let NoNetworkError = "Unable to connect to internet"
    static let parsingDataErrorMessage = "Error occured while parsing the data"
    static let internetConnectionErrorMessage = "The Internet connection appears to be offline."
    static let invalidAPIKeysErrorMessage = "Invalid API Keys"
}


enum APIError: Error, Equatable {
    case invalidURL
    case networkError(String)
    case invalidResponse
    case decodingError(String)
}
