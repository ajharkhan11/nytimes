//
//  UIViewController+ActivityIndicator.swift
//  NYTimes
//
//  Created by Ajharudeen Khan on 12/01/24.
//

import Foundation
import UIKit

let activityIndicator = UIActivityIndicatorView.init(style: .large)

extension UIViewController {
    
    func startActivityIndicator() {
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.center = self.view.center
    }
    
    func stopActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    func presentAlert(withTitle title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
}
